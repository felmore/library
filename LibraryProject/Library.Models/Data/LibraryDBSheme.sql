
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 05/13/2013 17:50:39
-- Generated from EDMX file: D:\MyProjects\library\Library.ADANA\LibraryProject\Library.Models\LibraryModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Library];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AuthorBooks_Author]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AuthorBooks] DROP CONSTRAINT [FK_AuthorBooks_Author];
GO
IF OBJECT_ID(N'[dbo].[FK_AuthorBooks_Books]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AuthorBooks] DROP CONSTRAINT [FK_AuthorBooks_Books];
GO
IF OBJECT_ID(N'[dbo].[FK_BooksHistories_Books]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BooksHistories] DROP CONSTRAINT [FK_BooksHistories_Books];
GO
IF OBJECT_ID(N'[dbo].[FK_BooksHistory_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BooksHistories] DROP CONSTRAINT [FK_BooksHistory_User];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AuthorBooks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AuthorBooks];
GO
IF OBJECT_ID(N'[dbo].[Authors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Authors];
GO
IF OBJECT_ID(N'[dbo].[Books]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Books];
GO
IF OBJECT_ID(N'[dbo].[BooksHistories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BooksHistories];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Authors'
CREATE TABLE [dbo].[Authors] (
    [AuthorId] bigint IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Books'
CREATE TABLE [dbo].[Books] (
    [BookId] bigint IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [Status] int  NOT NULL,
    [FilePath] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'BooksHistories'
CREATE TABLE [dbo].[BooksHistories] (
    [HistoryId] bigint IDENTITY(1,1) NOT NULL,
    [UserId] bigint  NOT NULL,
    [BookId] bigint  NOT NULL,
    [TakenDate] datetime  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserId] bigint IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(50)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'AuthorBooks'
CREATE TABLE [dbo].[AuthorBooks] (
    [Authors_AuthorId] bigint  NOT NULL,
    [Books_BookId] bigint  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AuthorId] in table 'Authors'
ALTER TABLE [dbo].[Authors]
ADD CONSTRAINT [PK_Authors]
    PRIMARY KEY CLUSTERED ([AuthorId] ASC);
GO

-- Creating primary key on [BookId] in table 'Books'
ALTER TABLE [dbo].[Books]
ADD CONSTRAINT [PK_Books]
    PRIMARY KEY CLUSTERED ([BookId] ASC);
GO

-- Creating primary key on [HistoryId] in table 'BooksHistories'
ALTER TABLE [dbo].[BooksHistories]
ADD CONSTRAINT [PK_BooksHistories]
    PRIMARY KEY CLUSTERED ([HistoryId] ASC);
GO

-- Creating primary key on [UserId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [Authors_AuthorId], [Books_BookId] in table 'AuthorBooks'
ALTER TABLE [dbo].[AuthorBooks]
ADD CONSTRAINT [PK_AuthorBooks]
    PRIMARY KEY NONCLUSTERED ([Authors_AuthorId], [Books_BookId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [BookId] in table 'BooksHistories'
ALTER TABLE [dbo].[BooksHistories]
ADD CONSTRAINT [FK_BooksHistories_Books]
    FOREIGN KEY ([BookId])
    REFERENCES [dbo].[Books]
        ([BookId])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BooksHistories_Books'
CREATE INDEX [IX_FK_BooksHistories_Books]
ON [dbo].[BooksHistories]
    ([BookId]);
GO

-- Creating foreign key on [UserId] in table 'BooksHistories'
ALTER TABLE [dbo].[BooksHistories]
ADD CONSTRAINT [FK_BooksHistory_User]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_BooksHistory_User'
CREATE INDEX [IX_FK_BooksHistory_User]
ON [dbo].[BooksHistories]
    ([UserId]);
GO

-- Creating foreign key on [Authors_AuthorId] in table 'AuthorBooks'
ALTER TABLE [dbo].[AuthorBooks]
ADD CONSTRAINT [FK_AuthorBooks_Authors]
    FOREIGN KEY ([Authors_AuthorId])
    REFERENCES [dbo].[Authors]
        ([AuthorId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Books_BookId] in table 'AuthorBooks'
ALTER TABLE [dbo].[AuthorBooks]
ADD CONSTRAINT [FK_AuthorBooks_Books]
    FOREIGN KEY ([Books_BookId])
    REFERENCES [dbo].[Books]
        ([BookId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AuthorBooks_Books'
CREATE INDEX [IX_FK_AuthorBooks_Books]
ON [dbo].[AuthorBooks]
    ([Books_BookId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------