USE [Library]
GO
SET IDENTITY_INSERT [dbo].[Authors] ON 

INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (7, N'Михаил ', N'Булгаков')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (8, N'Федор', N'Достоевский ')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (9, N'Александр', N'Пушкин')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (10, N'Артур', N'Конан Дойль')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (11, N'Илья ', N'Ильф')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (12, N'Евгений ', N'Петров')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (13, N'Лев ', N'Толстой')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (14, N'Михаил ', N'Лермонтов')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (15, N'Антон ', N'Чехов')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (16, N'Александр ', N'Дюма ')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (17, N'Эрих ', N'Мария Ремарк')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (18, N'Оскар ', N'Уайльд')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (20, N'Александр ', N'Грибоедов')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (21, N'Маргарет ', N'Митчелл')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (22, N'Николай ', N'Гоголь')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (23, N'Уильям ', N'Шекспир')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (24, N'Михаил ', N'Шолохов')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (25, N'Джейн ', N'Остин')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (26, N'Марк ', N'Твен')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (27, N'Иван ', N'Тургенев')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (28, N'Даниель ', N'Дефо')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (29, N'Борис ', N'Васильев')
INSERT [dbo].[Authors] ([AuthorId], [FirstName], [LastName]) VALUES (30, N'Иван ', N'Гончаров')
SET IDENTITY_INSERT [dbo].[Authors] OFF
SET IDENTITY_INSERT [dbo].[Books] ON 

INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (2, N'Мастер и Маргарита', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (3, N'Преступление и наказание
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (4, N'Евгений Онегин
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (5, N'Приключения Шерлока Холмса
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (7, N'Собачье сердце
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (8, N'Двенадцать стульев
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (9, N'Война и мир
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (10, N'Герой нашего времени
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (11, N'Рассказы
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (12, N'Граф Монте-Кристо
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (13, N'Три товарища
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (14, N'Идиот
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (15, N'Портрет Дориана Грея
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (16, N'Горе от ума
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (17, N'Унесённые ветром
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (18, N'Мёртвые души
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (19, N'Анна Каренина
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (20, N'Ромео и Джульетта
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (21, N'Братья Карамазовы
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (22, N'Вечера на хуторе близ Диканьки
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (24, N'Тихий Дон
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (25, N'Три мушкетера
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (26, N'Гордость и предубеждение
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (27, N'Приключения Тома Сойера', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (28, N'Капитанская дочка
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (29, N'Отцы и дети
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (30, N'Робинзон Крузо
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (31, N'А зори здесь тихие
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (32, N'Обломов
', 1, N'c:\')
INSERT [dbo].[Books] ([BookId], [Title], [Status], [FilePath]) VALUES (33, N'Триумфальная арка
', 1, N'c:\')
SET IDENTITY_INSERT [dbo].[Books] OFF
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (7, 2)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (7, 7)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (8, 3)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (8, 14)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (8, 21)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (9, 4)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (9, 28)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (10, 5)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (11, 8)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (12, 8)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (13, 9)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (13, 19)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (14, 10)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (15, 11)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (16, 12)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (16, 25)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (17, 13)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (17, 33)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (18, 15)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (20, 16)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (21, 17)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (22, 18)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (22, 22)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (23, 20)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (24, 24)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (25, 26)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (26, 27)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (27, 29)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (28, 30)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (29, 31)
INSERT [dbo].[AuthorBooks] ([Authors_AuthorId], [Books_BookId]) VALUES (30, 32)
