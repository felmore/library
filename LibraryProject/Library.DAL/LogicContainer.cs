﻿// -----------------------------------------------------------------------
// <copyright file="LogicContainer.cs" company="">
// This file was be create by Felmore from ADANA team
// All rights reserved �2013
// </copyright>
// -----------------------------------------------------------------------

namespace Library.DAL
{
    using System;
    using Microsoft.Practices.Unity;
    using Ocorola.Module.Infrastructure.Abstractions;

    /// <summary>
    /// Provide LogicContainer functionality
    /// </summary>
    public class LogicContainer : IServiceProviderExt
    {
        #region Fields

        /// <summary>
        /// Mutex for lock function
        /// </summary>
        private static readonly object _mutex = new object();

        /// <summary>
        /// Instance of Logic container
        /// </summary>
        private static LogicContainer m_Instance = default(LogicContainer);

        /// <summary>
        /// Instance of Unity container
        /// </summary>
        private readonly IUnityContainer m_Container = default(IUnityContainer);

        #endregion

        #region Constructors

        /// <summary>
        /// Private constructor
        /// </summary>
        private LogicContainer()
        {
            this.m_Container = new UnityContainer();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets thread save instance 
        /// </summary>
        public static IServiceProviderExt Instance
        {
            get
            {
                lock (_mutex)
                {
                    if (m_Instance == null)
                    {
                        m_Instance = new LogicContainer();
                        m_Instance.Initialize();
                    }
                }

                return m_Instance;
            }
        }

        #endregion

        #region Public logic

        /// <summary>
        /// Get the Service
        /// </summary>
        /// <typeparam name="T">type</typeparam>
        /// <param name="parameters">constructor parameters</param>
        /// <returns>returned type</returns>
        public T GetService<T>(object[] parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the Service
        /// </summary>
        /// <typeparam name="T">type</typeparam>
        /// <returns>returned type</returns>
        public T GetService<T>()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the Service
        /// </summary>
        /// <param name="serviceType">type</param>
        /// <returns>object</returns>
        public object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private logic

        /// <summary>
        /// Initialize method
        /// </summary>
        private void Initialize()
        {
        }

        #endregion
    }
}
