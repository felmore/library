﻿/// -----------------------------------------------------------------------
/// <copyright file="ExampleDomainModel.cs" company="">
/// TODO: Update copyright text.
/// </copyright>
/// -----------------------------------------------------------------------

namespace $rootnamespace$.Models
{

    using System;
    using System.Collections.Generic;
    using System.Data.Services.Common;

    using System.Runtime.Serialization;

    using Ocorola.Entity.Infrastructure;
    using Ocorola.Module.Infrastructure.Models;

    /// <summary>
    /// Example domain model
    /// </summary>
    [DataContract, DataServiceEntity]
    public class ExampleDomainModel: DomainModelBase
    {
        [DataMember(IsRequired = true, Order = 1, Name = "customerId")]
        public Guid Id { get; set; }

        [DataMember(Order = 2, Name = "customerName")]
        public string StringProperty { get; set; }

        [DataMember(IsRequired = true, Order = 3, Name = "customerAmount")]
        public decimal DecimalProperty { get; set; }

        #region DomainModelBase implementation
        
        static ExampleDomainModel()
        {
            ///Register domain model into root serializaito service
            DomainModelBase.RegisterType<ExampleDomainModel>();
        }

        public override void AssignTo(object obj)
        {
            if (obj is ExampleDomainModel)
            {
                var example = obj as ExampleDomainModel;

                example.Id = Id;
                example.StringProperty = StringProperty;
                example.DecimalProperty = DecimalProperty;

                return;
            }
            throw new NotSupportedException();
        }

        public override bool IsKeyEquals(object key)
        {
            if (key is Guid)
                return Id.CompareTo((Guid)key) == 0;

            if (key is string)
            {
                var gResult = default(Guid);
                if(!Guid.TryParse((string)key , out gResult))
                    return false;

                return  Id.CompareTo(gResult) == 0;
            }
            return false;
        }

        #endregion
    }
}
