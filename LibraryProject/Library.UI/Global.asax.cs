﻿// -----------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="">
// This file was be create by amid from ADANA team
// All rights reserved �2013
// </copyright>
// -----------------------------------------------------------------------

namespace Library.UI
{
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Main MVC Application class
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Application start method
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}