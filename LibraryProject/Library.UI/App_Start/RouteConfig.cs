﻿// -----------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="">
// This file was be create by amid from ADANA team
// All rights reserved �2013
// </copyright>
// -----------------------------------------------------------------------

namespace Library.UI
{
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Class to configure Routing
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Register routes
        /// </summary>
        /// <param name="routes">Route parameters</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}