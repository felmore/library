﻿// -----------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="">
// This file was be create by amid from ADANA team
// All rights reserved �2013
// </copyright>
// -----------------------------------------------------------------------

namespace Library.UI
{
    using System.Web.Http;

    /// <summary>
    /// Configure Web controller
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Register Web Routing
        /// </summary>
        /// <param name="config">Configuration parameters</param>
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });
        }
    }
}
