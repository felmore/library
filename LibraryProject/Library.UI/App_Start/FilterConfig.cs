﻿// -----------------------------------------------------------------------
// <copyright file="FilterConfig.cs" company="">
// This file was be create by amid from ADANA team
// All rights reserved �2013
// </copyright>
// -----------------------------------------------------------------------

namespace Library.UI
{
    using System.Web.Mvc;

    /// <summary>
    /// Filter configuration class
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Register global filters
        /// </summary>
        /// <param name="filters">Global filter collection</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}